package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {
    private Label msgLbl;
    private Button helloBtn;
    private TextField msgTxtField;
    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("Hello JavaFx");
        helloBtn = new Button();
        msgTxtField = new TextField();
        msgLbl = new Label();
        msgLbl.setText("Don't click the button !!");
        msgTxtField.setPromptText("What your name?");

        helloBtn.setText("Click Me!");
        helloBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                String name = msgTxtField.getText().trim();
                msgLbl.setText("Hie " + name + " Nice to Meet you !!");
            }
        });
        FlowPane flowPaneRoot = new FlowPane(10, 10);
        flowPaneRoot.setAlignment(Pos.CENTER);
        //StackPane root = new StackPane();
        flowPaneRoot.getChildren().add(helloBtn);
        flowPaneRoot.getChildren().add(msgLbl);
        flowPaneRoot.getChildren().add(msgTxtField);
        Scene scene = new Scene(flowPaneRoot, 250, 200);
        primaryStage.setScene(scene);
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
